import React from 'react';
import { Route, Switch } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import { INDEX, PAGE } from './Component/Routes';
import Login from './Pages/Login';
import Pagination from './Pages/Pagnation';

class App extends React.Component<any> {
  public render() {
    return (
      <React.Fragment>
        <BrowserRouter>
          <Switch>
            <Route exact={true} path={INDEX} component={Login} />
            <Route exact={true} path={PAGE} component={Pagination} />
          </Switch>
        </BrowserRouter>
      </React.Fragment>
    );
  }
}

export default App;
