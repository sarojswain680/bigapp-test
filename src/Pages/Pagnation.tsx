
import { IntegratedFiltering, IntegratedPaging, IntegratedSorting, PagingState, SearchState, SortingState } from '@devexpress/dx-react-grid';
import { Grid, PagingPanel, SearchPanel, Table, TableHeaderRow, Toolbar } from '@devexpress/dx-react-grid-bootstrap4';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
const URL = 'https://jsonplaceholder.typicode.com/users'
interface IProps extends RouteComponentProps<any> {

}
interface IState {
    columns: { name: string, title: string, key?: string, getRowMetaData?: (row: string) => void }[]
    rows: any[]
    pageSize: number
    totalCount: number
    currentPage: number
    addressFormatted: any
    searchValue: string
    user: any[]
    isSortedAsc: boolean
}

class Pagination extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            columns: [
                { name: 'name', title: 'Full Name' },
                { name: 'username', title: 'User Name' },
                { name: 'phone', title: 'PHONE' },
                {
                    name: 'cn', title: 'Company'
                },
            ],
            rows: [],
            pageSize: 3,
            totalCount: 0,
            currentPage: 0,
            addressFormatted: '',
            searchValue: '',
            user: [],
            isSortedAsc: false
        };
        this.changeCurrentPage = this.changeCurrentPage.bind(this);
        this.changeSearchValue = this.changeSearchValue.bind(this);
    }

    changeCurrentPage(currentPage: number) {
        this.setState({
            currentPage,
        });
    }

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate() {
        this.loadData();
    }

    queryString() {
        const { pageSize, currentPage, searchValue, columns } = this.state;

        if (searchValue && columns) {
            let filter = columns.reduce((acc: any, { name }: any) => {
                acc.push(`["${name}", "contains", "${encodeURIComponent(searchValue)}"]`);
                return acc;
            }, []).join(',"or",');

            if (columns.length > 1) {
                filter = `[${filter}]`;
            }

            return `${URL}?filter=${filter}`;
        } else {
            return `${URL}?take=${pageSize}&skip=${pageSize * currentPage}`;
        }
    }
    compareBy(key: any) {
        return function (a: any, b: any) {
            if (a[key] < b[key]) return -1;
            if (a[key] > b[key]) return 1;
            return 0;
        };
    }

    unCompareByKey(key: any) {
        return function (a: any, b: any) {
            if (a[key] > b[key]) return -1;
            if (a[key] < b[key]) return 1;
            return 0;
        };
    }
    sortBy(key: any) {
        let newUser = [...this.state.user];
        this.state.isSortedAsc ? newUser.sort(this.unCompareByKey(key)) : newUser.sort(this.compareBy(key));
        this.setState({
            user: newUser,
            isSortedAsc: !this.state.isSortedAsc
        });
    }
    loadData() {
        const queryString = this.queryString();
        fetch(queryString, { mode: 'cors' })
            .then(response => response.json())
            .then(data => {

                for (let i = 0; i < data.length; i++) {
                    data[i].cn = data[i].company.name;
                }

                this.setState({
                    rows: data,
                    totalCount: data.totalCount,
                })
            })
            .catch(() => this.setState({}));
    }

    handleChange = (event: any) => {
        event.preventDefault()
        this.setState({ pageSize: parseInt(event.target.value) });
    }

    changeSearchValue(searchValue: any) {
        this.setState({
            searchValue: searchValue
        });
    }

    render() {
        const { rows, columns, pageSize, currentPage } = this.state;
        return (
            <div className="card" style={{ position: 'relative' }}>

                <Grid
                    rows={rows}
                    columns={columns}
                >
                    <PagingState
                        currentPage={currentPage}
                        onCurrentPageChange={this.changeCurrentPage}
                        pageSize={pageSize}
                        defaultCurrentPage={0}
                    />
                    <IntegratedPaging />
                    <SortingState
                        defaultSorting={[{ columnName: 'name', direction: 'asc' }]}
                    />
                    <IntegratedSorting />
                    <SearchState
                        value={this.state.searchValue}
                        onValueChange={this.changeSearchValue}
                    />
                    <IntegratedFiltering />
                    <Table />
                    <TableHeaderRow showSortingControls />
                    <Toolbar />
                    <PagingPanel />
                    <SearchPanel />
                </Grid>
                <div>
                    <span>Items per page : </span>
                    <select onChange={this.handleChange} value={pageSize}>
                        <option value={3}>3</option>
                        <option value={5}>5</option>
                        <option value={6}>6</option>
                    </select>
                </div>
            </div>
        );
    }
}
export default withRouter(Pagination);