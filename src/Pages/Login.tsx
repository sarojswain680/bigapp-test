import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { isValidEmail } from '../Component/HelperMethods';
import { PAGE } from '../Component/Routes';


interface IProps extends RouteComponentProps<any> {

}

interface IState {
    email_or_phone: string,
    password: string,
    error: null | string,
    isLoginBtnDisable: boolean
}

class Login extends React.Component<IProps, IState>{
    constructor(props: IProps) {
        super(props);

        this.state = {
            email_or_phone: "",
            password: '',
            error: null,
            isLoginBtnDisable: false,
        };
    }

    getValidationErr = (email_or_phone: string, password: string) => {
        let err = ''

        if (!email_or_phone) {
            return err = 'Enter email or phone number'
        }
        else if (!password) {
            return err = 'Enter password'
        } else if (password.length < 6) {
            return err = 'Invalid user name or password '
        }
        else if (email_or_phone.indexOf('@') > 0) {

            if (!isValidEmail(email_or_phone)) {
                return err = 'Enter valid email id / phone number'

            }
        }
        else if (isNaN(Number.parseInt(email_or_phone))) {
            return err = 'Invalid email or phone number'
        } else if (isNaN(Number.parseInt(email_or_phone))) {
            if (email_or_phone.length < 10) {
                return err = 'Mobile number must be 10 digits long'
            }
        }
        return err
    }

    handleLogin = (event: any) => {
        event.preventDefault()
        const { email_or_phone, password } = this.state;
        const err = this.getValidationErr(email_or_phone, password)

        if (err) {
            this.setState({ error: err })
            return
        }
        this.setState({ error: err, isLoginBtnDisable: true })

        if (email_or_phone === 'sarojswain680@gmail.com' && password === '1234567') {
            this.props.history.push(PAGE)
        } else {
            this.setState({
                error: 'Invalid credential',
                isLoginBtnDisable: false
            })
        }
    }

    render() {
        const { email_or_phone, password, error, isLoginBtnDisable } = this.state;
        return (
            <div className="container" >
                <div className='row justify-content-center'>
                    <div className="title">
                        <img src="/logo/apple-icon-60x60.png" width='60' height='60' alt='' />
                        <h1 text-align="center">BIGAPP-TEST</h1>
                    </div>
                </div>
                <div className='row justify-content-center'>
                    <div className="col-sm-4">
                        <div className='base-form'>
                            <div className="card">
                                <article className="card-body">
                                    <h4 className="card-title text-center mb-4 mt-1">Login</h4>
                                    <hr />
                                    {
                                        error
                                            ? <p className="text-danger text-center">{error}</p>
                                            : <p className="text-center">Please enter your email and password</p>
                                    }
                                    <form onSubmit={this.handleLogin}>
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text"> <i className="fa fa-user"></i> </span>
                                                </div>
                                                <input type='email' className="form-control" autoCapitalize="off" value={email_or_phone} placeholder="Email or Phone" onChange={(e) => this.setState({ email_or_phone: e.target.value })} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                                                </div>
                                                <input type='password' className="form-control" autoCapitalize="off" value={password} placeholder="******" onChange={(e) => this.setState({ password: e.target.value })} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <button type="submit" className="btn btn-primary btn-block" onClick={this.handleLogin} disabled={isLoginBtnDisable}>{isLoginBtnDisable ? "Loading..." : "Login"}</button>
                                        </div>
                                    </form>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default withRouter(Login);